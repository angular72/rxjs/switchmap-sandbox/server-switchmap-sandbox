package com.rxjs.operator.sandbox.service;

import com.rxjs.operator.sandbox.models.Product;

import java.util.List;

public interface ProductService {
    List<Product> getAll();
}
