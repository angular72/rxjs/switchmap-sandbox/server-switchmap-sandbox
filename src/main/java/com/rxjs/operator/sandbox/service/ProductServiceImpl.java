package com.rxjs.operator.sandbox.service;

import com.rxjs.operator.sandbox.models.Product;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class ProductServiceImpl implements ProductService {
    @Override
    public List<Product> getAll() {
        return List.of(
                new Product(UUID.randomUUID().toString(), "Product 1"),
                new Product(UUID.randomUUID().toString(), "Product 2"),
                new Product(UUID.randomUUID().toString(), "Product 3"),
                new Product(UUID.randomUUID().toString(), "Product 4"),
                new Product(UUID.randomUUID().toString(), "Product 5")
        );
    }
}
